#! /bin/bash

declare -x TMPDIR=/usr/local/share/media/ripper

declare -x _disk_image="${1:-/dev/cdrom}"
declare -x _x264_bitrate="${2:-900}"
declare -x _mp3_bitrate="${3-240}"
declare -x _title=$( basename "$_disk_image" ".iso" )

declare -x _dir=$( mktemp --tmpdir --directory "$_title.XXXX" )

# Pick the longest DVD title
#declare -x _dvd_title=$( lsdvd "$_disk_image" 2>&1 | \
#  grep -vE '^\s*$' | tail -n1 | awk '{ print $NF }' )

declare -x _dvd_title="${4:-1}"

cd $_dir

# x264 Encoding: Pass 1
printf "Performing Pass 1 of x264 encoding..."
mencoder -dvd-device "$_disk_image" "dvd://$_dvd_title" \
  -o /dev/null -nosound                                 \
  -ovc x264 -x264encopts "direct=auto:pass=1:turbo:bitrate=$_x264_bitrate:bframes=1:me=umh:partitions=all:trellis=1:qp_step=4:qcomp=0.7:direct_pred=auto:keyint=300" \
  -vf "scale=-1:-10,harddup" &>"$_dir/${_title}_x264_pass_1.log"
printf "\n"

# x264 Encoding: Pass 2
printf "Performing Pass 2 of x264 encoding..."
mencoder -dvd-device "$_disk_image" "dvd://$_dvd_title" \
  -o "$_dir/${_title}_video.avi" -nosound               \
  -ovc x264 -x264encopts "direct=auto:pass=2:bitrate=$_x264_bitrate:bframes=1:me=umh:partitions=all:trellis=1:qp_step=4:qcomp=0.7:direct_pred=auto:keyint=300" \
  -vf "scale=-1:-10,harddup" &>"$_dir/${_title}_x264_pass_2.log"
printf "\n"

# MP3 Encoding
printf "Ripping the audio..."
mencoder -dvd-device "$_disk_image" "dvd://$_dvd_title" \
  -o "$_dir/${_title}_audio.avi" -ovc frameno           \
  -oac mp3lame -lameopts "br=${_mp3_bitrate}:cbr" -channels 6 -srate 48000 &>"$_dir/${_title}_mp3.log"
printf "\n"

#printf "Ripping subtitles..."
#printf "\n"

printf "Creating the final MKV file..."
mkvmerge -S -D "$_dir/${_title}_audio.avi" -S -A "$_dir/${_title}_video.avi" -o "$_dir/$_title.mkv"
printf "\n"

cd -

